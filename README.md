# Customer UI
* This project is the frontend for a RESTAPI backend application written in Java 8 and Spring Boot 2.3.4, which implements basic CRUD operations on a Customer scenario.
* You can view, create, update and delete any customer you want 😃
* It implements a mat-table with server side pagination, sorting and filtering
* It is written in Angular 10 and Angular Material.
* This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.7.

## How to run it
* install **nodejs** 12.+ and **npm** 6.+
* install **angular/cli** globally which will retrieve the latest Angular
* run **npm install** inside this project
* make sure the backend project is up and running
* run **ng serve --proxy-config proxy.config.json** or the start configuration from your IDE
* open your browser on http://localhost:4200/

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# REST API frontend Customer scenario

import {Component, Inject, OnInit} from '@angular/core';
import {ConfirmDialogData} from './confirm-dialog.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
    selector: 'app-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

    title: string;
    messageText: string;
    mainButtonText: string;

    constructor(private dialogRef: MatDialogRef<ConfirmDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public confirmDialogData: ConfirmDialogData) {
    }

    ngOnInit(): void {
        this.title = this.confirmDialogData.title;
        this.mainButtonText = this.confirmDialogData.mainButtonText;
        this.messageText = this.confirmDialogData.message;
    }

    primaryButtonHandler(): void{
        this.dialogRef.close(true);

    }

    close(): void{
        this.dialogRef.close(false);
    }


}

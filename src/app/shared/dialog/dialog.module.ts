import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AngularModule } from '../angular/angular.module';

@NgModule({
  declarations: [
    ConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    AngularModule
  ],
  exports: [
    ConfirmDialogComponent
  ]
})
export class DialogModule { }

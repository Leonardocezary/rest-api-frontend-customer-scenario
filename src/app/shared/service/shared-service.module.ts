import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MessageTransferService} from './message-transfer.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    MessageTransferService
  ]
})
export class SharedServiceModule { }

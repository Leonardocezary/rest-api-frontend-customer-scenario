import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MessageTransferService {

  private messageSubject = new BehaviorSubject(undefined);
  currentMessage = this.messageSubject.asObservable();

  constructor() { }

  sendMessage(message: string): void {
    this.messageSubject.next(message);
  }
}

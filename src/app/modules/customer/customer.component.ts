import {Component, Input, OnInit, ViewChildren} from '@angular/core';
import {CustomerTableComponent} from './components/customer-table/customer-table.component';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  @Input()
  width: string;
  @Input()
  minWidth: string;
  @Input()
  maxWidth: string;
  @ViewChildren(CustomerTableComponent) customerTableComponent: CustomerTableComponent;

  constructor() {
  }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerComponent } from './customer.component';
import { AngularModule } from '../../shared/angular/angular.module';
import { CustomerService } from './customer.service';
import { CustomerHeaderComponent } from './components/customer-header/customer-header.component';
import { CustomerInputSearchComponent } from './components/customer-input-search/customer-input-search.component';
import { CustomerTableComponent } from './components/customer-table/customer-table.component';
import { SharedServiceModule  } from '../../shared/service/shared-service.module';
import { CustomerDialogComponent } from './components/customer-dialog/customer-dialog.component';
import { DialogModule } from '../../shared/dialog/dialog.module';

@NgModule({
  declarations: [
    CustomerComponent,
    CustomerHeaderComponent,
    CustomerInputSearchComponent,
    CustomerTableComponent,
    CustomerDialogComponent
  ],
  imports: [
    CommonModule,
    AngularModule,
    SharedServiceModule,
    DialogModule
  ],
  exports: [
    CustomerComponent
  ],
  providers: [
    CustomerService
  ]
})
export class CustomerModule { }

import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Customer, CustomerPage} from './models/customer.model';
import {Observable, throwError} from 'rxjs';
import {CustomerCreateRequest, CustomerDeleteRequest, CustomerUpdateRequest} from './models/customer-request.model';
import {CustomerCreateResponse, CustomerDeleteResponse, CustomerUpdateResponse} from './models/customer-response.model';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }

  baseUrl = '/api/v1/customer/';

  /**
   *
   * @param customerId - the id of the customer
   */
  public getCustomerById(customerId: number): Observable<Customer> {
    const url = this.baseUrl + customerId;
    const options = {
      withCredentials: true
    };
    return this.httpClient.get<Customer>(url, options);
  }

  /**
   *
   * @param customerCreateRequest - the object which contains all the creation request data
   */
  public createCustomer(customerCreateRequest: CustomerCreateRequest): Observable<CustomerCreateResponse> {
    const url = this.baseUrl + '/add';
    const options = {
      withCredentials: true
    };
    return this.httpClient.post<CustomerCreateResponse>(url, customerCreateRequest, options);
  }

  /**
   *
   * @param customerUpdateRequest - the object which contains all the update request data
   */
  public updateCustomer(customerUpdateRequest: CustomerUpdateRequest): Observable<CustomerUpdateResponse> {
    const url = this.baseUrl + '/update';
    const options = {
      withCredentials: true
    };
    return this.httpClient.put<CustomerUpdateResponse>(url, customerUpdateRequest, options);
  }

  /**
   *
   * @param customerDeleteRequest - the object which contains all the delete request data
   */
  public deleteCustomer(customerDeleteRequest: CustomerDeleteRequest): Observable<CustomerDeleteResponse> {
    const url = this.baseUrl + '/delete';
    const options = {
      withCredentials: true,
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: customerDeleteRequest
    };
    return this.httpClient.delete<CustomerDeleteResponse>(url, options);
  }

  // tslint:disable-next-line:max-line-length
  public getPageableCustomers(pageNumber: number, pageSize: number, filterString: string, sortProperty: string, sortAscending: boolean): Observable<CustomerPage> {

    const url = this.baseUrl + '/customers';
    // Initialize Params Object
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append('page', pageNumber.toString());
    params = params.append('size', pageSize.toString());
    if (filterString) {
      params = params.append('filterString', filterString);
    }
    if (sortProperty) {
      params = params.append('sortProperty', sortProperty);
    }
    params = params.append('sortAscending', (sortAscending) ? sortAscending.toString() : 'false');

    return this.httpClient.get<CustomerPage>(url, { params })
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  /**
   *
   * @param error
   * @private
   */
  private handleError(error: HttpErrorResponse): Observable<any> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}

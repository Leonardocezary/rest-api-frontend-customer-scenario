import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, of} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {Customer, CustomerPage} from './models/customer.model';
import {CustomerService} from './customer.service';

export class CustomerDatasource implements DataSource<Customer> {

  private customersSubject = new BehaviorSubject<Customer[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  currentSelectedPage: number = 0;
  totalPages: number = 0;
  totalElements: number = 0;
  size: number = 0;
  customers: Array<Customer> = [];
  pageIndexes: Array<number> = [];
  filterString: string;
  // sorting
  sortProperty: string;
  sortAscending: boolean = false;

  constructor(private customerService: CustomerService) {
  }

  loadPageableCustomers(pageNumber: number, pageSize: number, filter?: string, sortProperty?: string, sortAscending?: boolean): void {
    this.loadingSubject.next(true);
    this.customerService.getPageableCustomers(pageNumber, pageSize, filter, sortProperty, sortAscending)
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(
        (customerPage: CustomerPage) => {
          console.log(customerPage);
          this.customers = customerPage.content;
          this.totalPages = customerPage.totalPages;
          this.pageIndexes = Array(this.totalPages).fill(0).map((x, i) => i);
          this.currentSelectedPage = customerPage.number;
          this.totalElements = customerPage.totalElements;
          this.size = customerPage.size;
          this.customersSubject.next(this.customers);
        },
        (error) => {
          console.log(error);
        }
      );
  }

  connect(collectionViewer: CollectionViewer): Observable<Customer[]> {
    console.log('Connecting data source');
    return this.customersSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.customersSubject.complete();
    this.loadingSubject.complete();
  }

}

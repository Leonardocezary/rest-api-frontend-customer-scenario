import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CustomerDialogData} from './customer-dialog-data.model';

@Component({
  selector: 'app-customer-dialog',
  templateUrl: './customer-dialog.component.html',
  styleUrls: ['./customer-dialog.component.scss']
})
export class CustomerDialogComponent implements OnInit {
  title: string;
  mainButtonText: string;
  form: FormGroup;
  editMode: boolean;
  // fields
  firstName: string;
  lastName: string;
  city: string;
  state: string;
  zip: string;
  phoneNumber: string;
  sexType: string;
  age: number;

  constructor(private formBuilder: FormBuilder, private dialogRef: MatDialogRef<CustomerDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public customerData: CustomerDialogData) { }

  ngOnInit(): void {
    this.initData();

    this.form = this.formBuilder.group({
      firstName: new FormControl(this.firstName, [Validators.required, Validators.maxLength(50)]),
      lastName: new FormControl(this.lastName, [Validators.required, Validators.maxLength(50)]),
      city: new FormControl(this.city, [Validators.required, Validators.maxLength(50)]),
      state: new FormControl(this.state, [Validators.required, Validators.maxLength(50)]),
      zip: new FormControl(this.zip, [Validators.required, Validators.maxLength(50)]),
      phoneNumber: new FormControl(this.phoneNumber, [Validators.required, Validators.maxLength(50)]),
      sexType: new FormControl(this.sexType, [Validators.required, Validators.maxLength(50)]),
      age: new FormControl(this.age, [Validators.required, Validators.max(150)])
    });

  }

  private initData(): void {
    this.firstName = this.customerData.firstName;
    this.lastName = this.customerData.lastName;
    this.city = this.customerData.city;
    this.state = this.customerData.state;
    this.zip = this.customerData.zip;
    this.phoneNumber = this.customerData.phoneNumber;
    this.sexType = this.customerData.sexType;
    this.age = this.customerData.age;
    this.title = this.customerData.title;
    this.mainButtonText = this.customerData.mainButtonText;
    this.editMode = this.customerData.editMode;
  }

  primaryButtonHandler(): void {
    if (this.form.valid) {
      if (this.editMode){
        this.form.value.customerId = this.customerData.customerId;
      }
      this.dialogRef.close(this.form.value);
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  isFormValid(form: FormGroup): boolean {
    return !form.pristine && form.valid;
  }
}

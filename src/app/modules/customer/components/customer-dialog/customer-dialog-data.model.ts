export class CustomerDialogData {
  customerId?: number;
  firstName: string;
  lastName: string;
  city: string;
  state: string;
  zip: string;
  phoneNumber: string;
  sexType: string;
  age: number;
  title: string;
  mainButtonText: string;
  editMode: boolean;
}

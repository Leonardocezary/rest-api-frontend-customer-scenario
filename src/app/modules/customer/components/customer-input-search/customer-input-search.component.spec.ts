import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerInputSearchComponent } from './customer-input-search.component';

describe('CustomerInputSearchComponent', () => {
  let component: CustomerInputSearchComponent;
  let fixture: ComponentFixture<CustomerInputSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerInputSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerInputSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MessageTransferService} from '../../../../shared/service/message-transfer.service';

@Component({
  selector: 'app-customer-input-search',
  templateUrl: './customer-input-search.component.html',
  styleUrls: ['./customer-input-search.component.scss']
})
export class CustomerInputSearchComponent implements OnInit {

  @Input()
  width: string;
  @Input()
  minWidth: string;
  @Input()
  maxWidth: string;

  @ViewChild
  ('documentInputHTML') documentInput: ElementRef<HTMLInputElement>;

  constructor(private messageTransferService: MessageTransferService) {
  }

  ngOnInit(): void {
  }

  reset(): void {
    this.documentInput.nativeElement.value = '';
    this.messageTransferService.sendMessage(this.documentInput.nativeElement.value);
  }

  handleInputChange(): void {
    this.messageTransferService.sendMessage(this.documentInput.nativeElement.value);
  }
}


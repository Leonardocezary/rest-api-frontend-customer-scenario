import {Component, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {CustomerDialogComponent} from '../customer-dialog/customer-dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CustomerService} from '../../customer.service';
import {MessageTransferService} from '../../../../shared/service/message-transfer.service';

@Component({
  selector: 'app-customer-header',
  templateUrl: './customer-header.component.html',
  styleUrls: ['./customer-header.component.scss']
})
export class CustomerHeaderComponent implements OnInit {

  constructor(private dialog: MatDialog, private snackBar: MatSnackBar,
              private customerService: CustomerService, private messageTransferService: MessageTransferService) {
  }

  ngOnInit(): void {
  }

  addCustomer(): void {
    const createDialog = new MatDialogConfig();
    createDialog.width = '30%';
    createDialog.disableClose = false;
    createDialog.autoFocus = true;
    createDialog.data = {
      title: 'Create customer',
      mainButtonText: 'Create'
    };
    const confirmDialogRef = this.dialog.open(CustomerDialogComponent, createDialog);
    confirmDialogRef.afterClosed().subscribe(
      customerCreateRequest => {
        if (customerCreateRequest) {
          this.customerService.createCustomer(customerCreateRequest)
            .subscribe(customerCreateResponse => {
              if (customerCreateResponse.createSucceeded) {
                this.messageTransferService.sendMessage('');
                this.snackBar.open('The customer was created!', 'X', {
                  duration: 10000,
                  panelClass: ['snackBarColor']
                });
              }
              else {
                this.snackBar.open('There\'s been an error while creating the customer!', 'X', {
                  duration: 10000,
                  panelClass: ['snackBarColor']
                });
              }
            });
        }
      }
      ,
      error => {
        this.snackBar.open('There\'s been an error while creating the customer!', 'X', {
          duration: 10000,
          panelClass: ['snackBarColor']
        });
      });
  }
}

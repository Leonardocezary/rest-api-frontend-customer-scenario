import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {merge} from 'rxjs';
import {CustomerDatasource} from '../../customer.datasource';
import {CustomerService} from '../../customer.service';
import {tap} from 'rxjs/operators';
import {MessageTransferService} from '../../../../shared/service/message-transfer.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ConfirmDialogComponent} from '../../../../shared/dialog/confirm-dialog/confirm-dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Customer} from '../../models/customer.model';
import {CustomerDeleteRequest} from '../../models/customer-request.model';
import {CustomerDialogComponent} from '../customer-dialog/customer-dialog.component';

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.scss']
})
export class CustomerTableComponent implements OnInit, AfterViewInit {
  customerDataSource: CustomerDatasource;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['firstName', 'lastName', 'city', 'state', 'zip', 'phoneNumber', 'sex', 'age', 'actions'];
  isLoading: boolean = true;
  pageSize: number = 2;
  filterString: string;

  constructor(private customerService: CustomerService, private messageTransferService: MessageTransferService,
              private dialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.customerDataSource = new CustomerDatasource(this.customerService);
  }

  ngAfterViewInit(): void {
    this.messageTransferService.currentMessage.subscribe(message => {
      this.filterString = message;
      this.paginator.pageIndex = 0;
      this.paginator.pageSize = this.pageSize;
      this.loadCustomerPage();
      this.isLoading = false;
    });
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadCustomerPage())
      )
      .subscribe();
  }

  loadCustomerPage(): void {
    this.customerDataSource.loadPageableCustomers(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filterString,
      this.sort.active,
      this.sort.direction === 'asc'
    );
  }

  confirmCustomerDeletion(customer: Customer): void {
    const confirmDialog = new MatDialogConfig();
    confirmDialog.width = '400px';
    confirmDialog.autoFocus = false;
    confirmDialog.data = {
      title: 'Remove customer',
      message: 'Are you sure you want to remove this customer?',
      mainButtonText: 'Remove'
    };
    const confirmDialogRef = this.dialog.open(ConfirmDialogComponent, confirmDialog);
    confirmDialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          this.customerService.deleteCustomer(new CustomerDeleteRequest(customer.customerId))
            .subscribe(value => {
              this.loadCustomerPage();
              this.snackBar.open('The customer was deleted!', 'X', {
                duration: 10000,
                panelClass: ['snackBarColor']
              });
            });
        }
      }
      ,
      error => {
        this.snackBar.open('There\'s been an error while removing the customer!', 'X', {
          duration: 10000,
          panelClass: ['snackBarColor']
        });
      });
  }

  editCustomer(customer: Customer): void {
    const editDialog = new MatDialogConfig();
    editDialog.width = '30%';
    editDialog.disableClose = false;
    editDialog.autoFocus = true;
    editDialog.data = {
      customerId: customer.customerId,
      firstName: customer.firstName,
      lastName: customer.lastName,
      city: customer.city,
      state: customer.city,
      zip: customer.zip,
      phoneNumber: customer.phoneNumber,
      sexType: customer.sexType,
      age: customer.age,
      title: 'Edit customer',
      mainButtonText: 'Edit',
      editMode: true
    };
    const confirmDialogRef = this.dialog.open(CustomerDialogComponent, editDialog);
    confirmDialogRef.afterClosed().subscribe(
      customerUpdateRequest => {
        if (customerUpdateRequest) {
          this.customerService.updateCustomer(customerUpdateRequest)
            .subscribe(customerUpdateResponse => {
              if (customerUpdateResponse && customerUpdateResponse.updateSucceeded) {
                this.loadCustomerPage();
                this.snackBar.open('The customer was edited!', 'X', {
                  duration: 10000,
                  panelClass: ['snackBarColor']
                });
              }
              else {
                this.snackBar.open('There\'s been an error while editing the customer!', 'X', {
                  duration: 10000,
                  panelClass: ['snackBarColor']
                });
              }
            });
        }
      }
      ,
      error => {
        this.snackBar.open('There\'s been an error while editing the customer!', 'X', {
          duration: 10000,
          panelClass: ['snackBarColor']
        });
      });
  }
}

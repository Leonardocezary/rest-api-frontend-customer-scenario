export class CustomerUpdateRequest {
  customerId: number;
  firstName: string;
  lastName: string;
  city: string;
  state: string;
  zip: string;
  phoneNumber: string;
  sexType: string;
  age: number;
}

export class CustomerCreateRequest {
  firstName: string;
  lastName: string;
  city: string;
  state: string;
  zip: string;
  phoneNumber: string;
  sexType: string;
  age: number;
}

export class CustomerDeleteRequest {
  constructor(private customerId: number) {}
}

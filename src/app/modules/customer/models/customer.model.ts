export class Customer {
  customerId: number;
  firstName: string;
  lastName: string;
  city: string;
  state: string;
  zip: string;
  phoneNumber: string;
  sexType: string;
  age: number;
}

export class CustomerPage {
  content: Customer[];
  totalPages: number;
  totalElements: number;
  number: number;
  size: number;
}

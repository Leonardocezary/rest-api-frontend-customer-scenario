export class CustomerCreateResponse {
  createSucceeded: boolean;
  errorMessage: string;
}

export class CustomerUpdateResponse {
  updateSucceeded: boolean;
  errorMessage: string;
}

export class CustomerDeleteResponse {
  deleteSucceeded: boolean;
  errorMessage: string;
}
